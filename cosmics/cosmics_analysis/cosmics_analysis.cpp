// STL dependencies
#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

// ROOT dependencies
#include "TCanvas.h"
#include "TF1.h"
#include "TMath.h"
#include "TH1I.h"
#include "TROOT.h"
#include "TStyle.h"

// using directives
using std::cerr;
using std::cout;
using std::endl;
using std::getline;
using std::ifstream;
using std::istringstream;
using std::reverse;
using std::string;
using std::vector;

int cosmics_analysis()
{
	// define storage for bin width and read values
	// (charge in x, counts in y)
	Double_t binWidth;

	vector<Double_t> xArr;
	vector<UInt_t> yArr;

	// read file
	{
		// open file and check if it could be opened
		ifstream inputFile("data/pmt07_front.txt");

		if (!inputFile.is_open())
		{
			cerr << "Error - File not found!" << endl;
			return 1;
		}

		// define objects for file reading
		string currLineRaw;
		istringstream currLine;

		Double_t charge;
		UInt_t counts;

		// skip header
		getline(inputFile, currLineRaw);

		// read data
		while (getline(inputFile, currLineRaw))
		{
			currLine.str((currLineRaw+" ").c_str());
			if (!(currLine >> charge >> counts))
			{
				cerr << "Error - Error when reading file!" <<
				        endl;
				return 2;
			}

			xArr.push_back(charge);
			yArr.push_back(counts);
		}

		inputFile.close();
	}

	// get array size
	UInt_t dataSize = xArr.size();

	if (dataSize >= 2)
	{
		binWidth = xArr[1]-xArr[0];
	}
	else
	{
		cerr << "Error - Cannot determine bin width!" << endl;
		return 3;
	}

	// use charge value array to set lower bin edges
	// (reverse order)
	for (vector<Double_t>::iterator iter = xArr.begin();
	     iter != xArr.end(); ++iter)
		*iter = -(*iter+binWidth/2);

	xArr.push_back(xArr.back()-binWidth);

	reverse(xArr.begin(), xArr.end());

	cout << xArr.size() << endl;

	// set plotting options before drawables are created
	gStyle->SetOptStat(0);
	gStyle->SetOptFit(1);
	
	// fill data into ROOT histogram (reverse order)
	TH1I *dataHist = new TH1I("dataHist", "cosmics spectrum;"
	                          "voltage*time [V*ns];counts",
	                          dataSize, &xArr[0]);

	for (UInt_t i = dataSize; i >= 1; --i)
		dataHist->SetBinContent(dataSize-i+1, yArr[i-1]);

	// draw histogram
	TCanvas *paintbrd = new TCanvas("paintbrd", "paintboard", 1000, 900);
	paintbrd->cd();

	dataHist->Draw();

	// define fit function and fit it to histogram
	TF1 *customGaus = new TF1("custom_gaus",
	                          "x<[1]?[0]*exp(-((x-[1])/[2])**2/2)+[4]:"
	                          "[0]*exp(-((x-[1])/[3])**2/2)+[4]");

	customGaus->SetParName(0, "Constant");
	customGaus->SetParName(1, "Mean");
	customGaus->SetParName(2, "Sigma 1");
	customGaus->SetParName(3, "Sigma 2");
	customGaus->SetParName(4, "Y Offset");

	customGaus->SetParameter("Constant", 200);
	customGaus->SetParameter("Mean", 0.11e-6);
	customGaus->SetParameter("Sigma 1", 0.01e-6);
	customGaus->SetParameter("Sigma 2", 0.02e-6);
	customGaus->SetParameter("Y Offset", 0);

	dataHist->Fit("custom_gaus", "", "", 0.08e-6, 0.13e-6);

	cout << "  CHISQUARE/NDF: " << customGaus->GetChisquare() << "/" <<
	        customGaus->GetNDF() << endl;
	cout << "  PVALUE: " << customGaus->GetProb() << endl;

	//customGaus->SetRange(0.1e-6, 0.135e-6);
	//customGaus->Draw("SAME");

	return 0;
}
