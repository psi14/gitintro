reset

set title "{/Symbol s}_2 of cosmic peaks (front of calo.)"
set xlabel "PMT no."
set ylabel "voltage*time [A.U.]"

set xrange [0:8]

set xtics 1,1,7 nomirror

set format y "%.2e"

set term pngcairo size 1280,1024 enhanced font "Verdana, 15"
set output "cosmic_sig2_front.png"

plot "pmt_cosmics_data.txt" using 1:6:7 with errorbars notitle lw 2 lc rgb "blue"