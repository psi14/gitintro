close all;clear all;
%CONSTANTS
c=3e8;%[m/s]
mpion=140;%[MeV]
mmuon=106;%[MeV]
melectron=0.5;%[MeV]
perA=19.75;%[s]

%Properties
DISTANCE=[23.65 23];
MRange=(150:220);

%Functions
traveltime=@(p,m,distance) distance.*sqrt(1+(p./m).^2)./(p/m)./c*1e9;
traveltimePer=@(p,m,distance) mod(traveltime(p,m,distance),19.75);

ttp=@(p,d) traveltimePer(p,mpion,d);
ttm=@(p,d) traveltimePer(p,mmuon,d);
fmin=@(p,d) min(min(abs(ttp(p,d)-ttm(p,d)),abs(ttm(p,d)+perA-ttp(p,d))),abs(ttm(p,d)-perA-ttp(p,d)));


%figures

c=colormap(jet(6));


figure(1)
for i=1:2
h(i*1)=plot(MRange,traveltime(MRange,mpion,DISTANCE(i)),'Color',c(i*1,:));hold on;
h(i*2)=plot(MRange,traveltime(MRange,mmuon,DISTANCE(i)),'Color',c(i*2,:));
h(i*3)=plot(MRange,traveltime(MRange,melectron,DISTANCE(i)),'Color',c(i*3,:));
end
title('Flight time particles');
xlabel('Momentum Range [MeV]');
ylabel('travel time [ns]');
legend(h,'pion fp','muon fp','electron fp','pion ','muon','electron');

%Calculate 19.75/2 difference between muon and pion

figure(2)


plot(MRange,fmin(MRange,DISTANCE(1)),'Color',c(1,:));hold on;
plot(MRange,fmin(MRange,DISTANCE(2)),'Color',c(2,:));
title('minimum distance of pion and muon')
ylabel('minimum time difference [ns]')
xlabel('Momentum Range [MeV]'); 
legend('Focus Point','Before Absorber');
ylim([0,10]);
% [r,a]=fmaxbnd(fmin,160,250);
% unterschied= @(x) abs(traveltime(x,mpion,DISTANCE)-traveltime(x,mmuon,DISTANCE));
% diff=unterschied(r);
% plot(MRange,unterschied(MRange),'k');